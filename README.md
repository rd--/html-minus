html-minus
----------

a negative
[haskell](https://haskell.org/)
[html5](https://www.w3.org/TR/html5/)
library

`Text.Html.Minus` is a very simple Haskell module
for writing html5 documents.  It provides
constructors for html5 elements and attributes
in terms of
[Text.XML.Light](https://hackage.haskell.org/package/xml).

© [rohan drape](https://rohandrape.net/),
  2006-2024,
  [gpl](https://gnu.org/copyleft/)

* * *

```
$ make doctest
Examples: 16  Tried: 16  Errors: 0  Failures: 0
$
```
