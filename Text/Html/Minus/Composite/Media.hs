module Text.Html.Minus.Composite.Media where

import qualified Data.Char as C {- base -}
import Data.Maybe {- base -}
import System.FilePath {- filepath -}
import Text.Printf {- base -}

import qualified Text.Html.Minus.Attribute as Html {- html-minus -}
import qualified Text.Html.Minus.Composite as Html {- html-minus -}
import qualified Text.Html.Minus.Element as Html {- html-minus -}

-- | (width,height)
type Dimensions = (Maybe Int, Maybe Int)

-- | Make 'Html.width' & 'Html.height' attributes if given.
dimensions_attr :: Dimensions -> [Html.Attr]
dimensions_attr (w, h) =
  let w' = fmap (Html.width . show) w
      h' = fmap (Html.height . show) h
  in catMaybes [w', h']

-- | 'Html.video' element embedding local video.
mk_video :: Dimensions -> FilePath -> Html.Content
mk_video dm fn = Html.video (Html.controls : Html.src fn : dimensions_attr dm) []

mk_video_iframe :: String -> String -> Dimensions -> Html.Content
mk_video_iframe k ln dm =
  let a = Html.id k : Html.src ln : Html.allowfullscreen : Html.frameborder "0" : dimensions_attr dm
  in Html.iframe a []

{- | 'Html.iframe' element embedding @YouTube@ video.
The element ID is the video ID with a @yt_@ prefix.
-}
mk_video_yt :: Dimensions -> String -> Html.Content
mk_video_yt dm n =
  let ln = "https://www.youtube.com/embed/" ++ n
  in mk_video_iframe ("yt_" ++ n) ln dm

{- | 'Html.iframe' element embedding @vimeo@ video.
The element ID is the video ID with a @vimeo_@ prefix.
-}
mk_video_vimeo :: Dimensions -> String -> Html.Content
mk_video_vimeo dm n =
  let ln = "https://player.vimeo.com/video/" ++ n
  in mk_video_iframe ("vimeo_" ++ n) ln dm

data Video_Host = YouTube | Vimeo deriving (Eq, Show)

mk_video_host :: Dimensions -> (Video_Host, String) -> Html.Content
mk_video_host dm (hst, vid) =
  case hst of
    YouTube -> mk_video_yt dm vid
    Vimeo -> mk_video_vimeo dm vid

{- | Split video URL into host and identifier parts.

>>> video_url_analyse "http://vimeo.com/121005699"
Just (Vimeo,"121005699")

>>> video_url_analyse "http://youtube.com/watch?v=iK0dgc8FI0w"
Just (YouTube,"iK0dgc8FI0w")
-}
video_url_analyse :: String -> Maybe (Video_Host, String)
video_url_analyse u =
  let (r_vid, r_hst) = break (== '/') (reverse u)
      hst = map C.toLower (reverse r_hst)
  in case hst of
      "http://youtu.be/" -> Just (YouTube, reverse r_vid)
      "http://vimeo.com/" -> Just (Vimeo, reverse r_vid)
      "http://youtube.com/" ->
        case break (== '=') (reverse r_vid) of
          ("watch?v", _ : vid) -> Just (YouTube, vid)
          _ -> Nothing
      _ -> Nothing

-- | Analyse small set of URL types and make video frame.
mk_video_url_maybe :: Dimensions -> String -> Maybe Html.Content
mk_video_url_maybe dm = fmap (mk_video_host dm) . video_url_analyse

{- | Analyse small set of URL types and make video frame.

>>> import Text.Html.Minus.Render
>>> pp = putStr . showHtml5 . mk_video_url (Nothing,Nothing)
>>> pp "http://vimeo.com/121005699"
<iframe id="vimeo_121005699" src="https://player.vimeo.com/video/121005699" allowfullscreen="allowfullscreen" frameborder="0"></iframe>

>>> pp "http://youtu.be/iK0dgc8FI0w"
<iframe id="yt_iK0dgc8FI0w" src="https://www.youtube.com/embed/iK0dgc8FI0w" allowfullscreen="allowfullscreen" frameborder="0"></iframe>

>>> pp "http://youtube.com/watch?v=iK0dgc8FI0w"
<iframe id="yt_iK0dgc8FI0w" src="https://www.youtube.com/embed/iK0dgc8FI0w" allowfullscreen="allowfullscreen" frameborder="0"></iframe>
-}
mk_video_url :: Dimensions -> String -> Html.Content
mk_video_url dm u =
  let err = error ("mk_video_url: " ++ u)
  in fromMaybe err (mk_video_url_maybe dm u)

-- * Audio

{- | Derive @mimetype@ from file extension

>>> au_derive_mimetype "oblique.mp3"
"audio/mpeg"
-}
au_derive_mimetype :: FilePath -> String
au_derive_mimetype nm =
  case takeExtension nm of
    ".mp3" -> "audio/mpeg"
    _ -> error "au_derive_mimetype"

mcons :: Maybe a -> [a] -> [a]
mcons e l = maybe l (: l) e

-- | Make 'Html.audio' element.
au_gen_audio :: Bool -> String -> String -> Html.Content
au_gen_audio ctl k_au nm =
  let attr =
        mcons
          (if ctl then Just Html.controls else Nothing)
          [Html.id k_au, Html.preload "metadata"]
  in Html.audio attr [Html.source [Html.src nm, Html.type_attr (au_derive_mimetype nm)]]

{- | Au Gen Js

>>> putStr $ unlines $ au_gen_js "K" ("P","S")
var au_K = document.getElementById('au_K');
var ctl_K = document.getElementById('ctl_K');
console.log('au_gen_js: K');
if (ctl_K == null) {
  console.log('ctl_K == null');
} else {
  ctl_K.onclick = function () {
    var st = ctl_K.innerHtml === 'S';
    ctl_K.innerHtml = st ? 'P' : 'S';
    au_K[st ? 'pause' : 'play']();
    return false;
  };
}
-}
au_gen_js :: String -> (String, String) -> [String]
au_gen_js k (p, s) =
  [ printf "var au_%s = document.getElementById('au_%s');" k k
  , printf "var ctl_%s = document.getElementById('ctl_%s');" k k
  , printf "console.log('au_gen_js: %s');" k
  , printf "if (ctl_%s == null) {" k
  , printf "  console.log('ctl_%s == null');" k
  , "} else {"
  , printf "  ctl_%s.onclick = function () {" k
  , printf "    var st = ctl_%s.innerHtml === '%s';" k s
  , printf "    ctl_%s.innerHtml = st ? '%s' : '%s';" k p s
  , printf "    au_%s[st ? 'pause' : 'play']();" k
  , "    return false;"
  , "  };"
  , "}"
  ]

au_gen_script :: String -> (String, String) -> Html.Content
au_gen_script k ch = Html.script_js (unlines (au_gen_js k ch))

au_gen_button :: String -> String -> Html.Content
au_gen_button k_ctl x = Html.button [Html.class_attr "ctl-au", Html.id k_ctl] [Html.cdata x]

au_gen_a :: String -> String -> Html.Content
au_gen_a k_ctl x = Html.a [Html.class_attr "ctl-au", Html.href "#", Html.id k_ctl] [Html.cdata x]

{-
import qualified Data.Digest.Murmur32 as H32 {- murmur-hash -}
murmur32_string :: String -> String
murmur32_string = flip showHex "" . H32.asWord32 . H32.hash32
-}

name_to_id :: String -> String
name_to_id = map (\c -> if C.isAlphaNum c then c else '_')

{- | Make audio element, perhaps with autoplay, with text nodes for
@play@ and @stop@ controls and required JS codes.  The @ID@ is a
derived from the file name.

> import Text.Html.Minus.Render
> putStr $ showHtml5 $ mk_audio (False,"L","#") "ev/w/tract/tract-malthouse.mp3"
-}
mk_audio :: (Bool, String, String) -> String -> Html.Content
mk_audio (ap, p, s) nm =
  let k = name_to_id nm
      k_au = "au_" ++ k
      k_ctl = "ctl_" ++ k
      ap_f = if ap then Html.x_add_attrs_content [Html.autoplay] else id
      au =
        [ ap_f (au_gen_audio False k_au nm)
        , au_gen_a k_ctl (if ap then s else p) -- au_gen_{a | button}
        , au_gen_script k (p, s)
        ]
  in Html.span [Html.class_attr "au"] au

-- * Media Text

{- | Split at

>>> map (split_at ':') (words "640:360 480:360 360:270 320:240 :360 640:")
[("640","360"),("480","360"),("360","270"),("320","240"),("","360"),("640","")]
-}
split_at :: Eq t => t -> [t] -> ([t], [t])
split_at c s =
  case span (/= c) s of
    (lhs, []) -> (lhs, [])
    (lhs, _ : rhs) -> (lhs, rhs)

{- | Dimensions given as @width:height@.

>>> map dimensions_parse (words "640:360 480:360 360:270 320:240 :360 640:")
[(Just 640,Just 360),(Just 480,Just 360),(Just 360,Just 270),(Just 320,Just 240),(Nothing,Just 360),(Just 640,Nothing)]
-}
dimensions_parse :: String -> Dimensions
dimensions_parse x =
  let read_or_null s = if null s then Nothing else Just (read s)
      (w, h) = split_at ':' x
  in (read_or_null w, read_or_null h)

{-

import Numeric {- base -}
import Data.List.Split {- split -}
import qualified Text.Html.Minus.Render as H {- html-minus -}

media_text_parse :: String -> Maybe Html.Content
media_text_parse s =
    case s of
      '<':'|':s' ->
           let dv cl = Html.div [Html.class_attr cl]
           in case splitOn "," s' of
                ["video",fn,z] -> Just (dv "video" [mk_video (dimensions_parse z) fn])
                ["vimeo",k,z] -> Just (dv "video" [mk_video_vimeo (dimensions_parse z) k])
                ["yt",k,z] -> Just (dv "video" [mk_video_yt (dimensions_parse z) k])
                ["audio",fn,pl,st] -> Just (dv "audio" [mk_audio (False,pl,st) fn])
                _ -> error ("media_text_parse: " ++ s)
      _ -> Nothing

-- | Inline text processor, filter for markdown texts etc.
--
-- > media_text_process "<|yt,bOKaMCTC07k,320:240"
-- > media_text_process "<|vimeo,121525441,500:281"
-- > media_text_process "<|video,r/023-022.mp4,320:240"
-- > media_text_process "<|audio,r/000-092.mp3,>,#"
-- > media_text_process "<|error"
-- > media_text_process "plain text"
media_text_process :: String -> String
media_text_process s = maybe s Html.showHtml5 (media_text_parse s)

-}
