-- | Menu constructors.
module Text.Html.Minus.Composite.Menu where

import Data.List as L {- base -}
import Prelude as P {- base -}

import qualified Text.Html.Minus.Attribute as H
import qualified Text.Html.Minus.Element as H

-- | A menu entry is /(name,identifier,link)/.
type Menu_Entry = (String, String, Maybe String)

type Menu_P = ([H.Content] -> [H.Content])

-- | Menu constructor.
type Menu_C = Menu_P -> String -> [Menu_Entry] -> String -> H.Content

nav_menu :: H.Element_C -> H.Element_C -> Menu_C
nav_menu outer_f inner_f between_f ty m h =
  let cl = H.class_attr ty
      f (nm, nm_id, ln) =
        let a_cl = H.class_attr (if nm_id == h then "here" else "not-here")
            at = maybe [a_cl] ((: [a_cl]) . H.href) ln
        in inner_f [cl, H.id nm_id] [H.a at [H.cdata nm]]
  in H.nav [cl] [outer_f [cl] (between_f (L.map f m))]

{- | Make a 'nav' menu of class /ty/ with a 'ul' structure.  The entry
corresponding to identifier /h/ is marked with the class @here@.
-}
nav_menu_list :: Menu_C
nav_menu_list = nav_menu H.ul H.li

-- | Variant of 'nav_menu_list' using 'H.span' elements.
nav_menu_span :: Menu_C
nav_menu_span f =
  let sp = H.span [H.class_attr "menu-separator"] [H.cdata " | "]
  in nav_menu H.div H.span (f . intersperse sp)
