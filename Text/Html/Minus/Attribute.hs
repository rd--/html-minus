{- | Html attribute constructors.  Where an attribute name conflicts
with a haskell keyword the attribute name is written with an @_attr@
suffix.  Where an attribute name conflicts with an
element name the attribute name is written likewise.
-}
module Text.Html.Minus.Attribute where

import qualified Text.XML.Light as X {- xml -}

type Attr = X.Attr

mk_attr :: String -> String -> Attr
mk_attr a = X.Attr (X.unqual a)

-- | Make an 'Attr' where the /key/ and /value/ are equal.
mk_bool_attr :: String -> Attr
mk_bool_attr a = mk_attr a a

accept :: String -> Attr
accept = mk_attr "accept"

accept_charset :: String -> Attr
accept_charset = mk_attr "accept-charset"

action :: String -> Attr
action = mk_attr "action"

align :: String -> Attr
align = mk_attr "align"

allowfullscreen :: Attr
allowfullscreen = mk_bool_attr "allowfullscreen"

alt :: String -> Attr
alt = mk_attr "alt"

autofocus :: Attr
autofocus = mk_bool_attr "autofocus"

autoplay :: Attr
autoplay = mk_bool_attr "autoplay"

bgcolor :: String -> Attr
bgcolor = mk_attr "bgcolor"

border :: String -> Attr
border = mk_attr "border"

charset :: String -> Attr
charset = mk_attr "charset"

-- | Suffixed since /class/ is a reserved word.
class_attr :: String -> Attr
class_attr = mk_attr "class"

classid :: String -> Attr
classid = mk_attr "classid"

cols :: String -> Attr
cols = mk_attr "cols"

colspan :: String -> Attr
colspan = mk_attr "colspan"

content :: String -> Attr
content = mk_attr "content"

controls :: Attr
controls = mk_bool_attr "controls"

coords :: String -> Attr
coords = mk_attr "coords"

currentTime :: String -> Attr
currentTime = mk_attr "currentTime"

-- | Suffixed since /data/ is a reserved word.
data_attr :: String -> Attr
data_attr = mk_attr "data"

datetime :: String -> Attr
datetime = mk_attr "datetime"

download :: String -> Attr
download = mk_attr "download"

enctype :: String -> Attr
enctype = mk_attr "enctype"

frameborder :: String -> Attr
frameborder = mk_attr "frameborder"

height :: String -> Attr
height = mk_attr "height"

href :: String -> Attr
href = mk_attr "href"

id :: String -> Attr
id = mk_attr "id"

http_equiv :: String -> Attr
http_equiv = mk_attr "http-equiv"

label_attr :: String -> Attr
label_attr = mk_attr "label"

lang :: String -> Attr
lang = mk_attr "lang"

language :: String -> Attr
language = mk_attr "language"

media :: String -> Attr
media = mk_attr "media"

method :: String -> Attr
method = mk_attr "method"

name :: String -> Attr
name = mk_attr "name"

onblur :: String -> Attr
onblur = mk_attr "onblur"

onchange :: String -> Attr
onchange = mk_attr "onchange"

onclick :: String -> Attr
onclick = mk_attr "onclick"

onfocus :: String -> Attr
onfocus = mk_attr "onfocus"

onkeydown :: String -> Attr
onkeydown = mk_attr "onkeydown"

onkeypress :: String -> Attr
onkeypress = mk_attr "onkeypress"

onkeyup :: String -> Attr
onkeyup = mk_attr "onkeyup"

onload :: String -> Attr
onload = mk_attr "onload"

onmousedown :: String -> Attr
onmousedown = mk_attr "onmousedown"

onmousemove :: String -> Attr
onmousemove = mk_attr "onmousemove"

onmouseout :: String -> Attr
onmouseout = mk_attr "onmouseout"

onmouseover :: String -> Attr
onmouseover = mk_attr "onmouseover"

onmouseup :: String -> Attr
onmouseup = mk_attr "onmouseup"

onmousewheel :: String -> Attr
onmousewheel = mk_attr "onmousewheel"

preload :: String -> Attr
preload = mk_attr "preload"

quality :: String -> Attr
quality = mk_attr "quality"

rel :: String -> Attr
rel = mk_attr "rel"

rows :: String -> Attr
rows = mk_attr "rows"

rowspan :: String -> Attr
rowspan = mk_attr "rowspan"

seamless :: Attr
seamless = mk_bool_attr "seamless"

selected :: Attr
selected = mk_bool_attr "selected"

shape :: String -> Attr
shape = mk_attr "shape"

-- | Suffixed since /span/ is the name of both an attribute and element.
span_attr :: String -> Attr
span_attr = mk_attr "span"

src :: String -> Attr
src = mk_attr "src"

-- | Suffixed since /style/ is the name of both an attribute and element.
style_attr :: String -> Attr
style_attr = mk_attr "style"

tabindex :: String -> Attr
tabindex = mk_attr "tabindex"

target :: String -> Attr
target = mk_attr "target"

-- | Suffixed since /title/ is the name of both an attribute and element.
title_attr :: String -> Attr
title_attr = mk_attr "title"

-- | Suffixed since /type/ is a reserved word.
type_attr :: String -> Attr
type_attr = mk_attr "type"

usemap :: String -> Attr
usemap = mk_attr "usemap"

user_data :: String -> String -> Attr
user_data k = mk_attr ("data-" ++ k)

valign :: String -> Attr
valign = mk_attr "valign"

value :: String -> Attr
value = mk_attr "value"

width :: String -> Attr
width = mk_attr "width"

xml_lang :: String -> Attr
xml_lang = mk_attr "xml:lang"

xmlns :: String -> Attr
xmlns = mk_attr "xmlns"
