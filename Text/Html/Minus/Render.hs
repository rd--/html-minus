module Text.Html.Minus.Render where

import qualified Text.XML.Light as X {- xml -}

import qualified Text.Html.Minus.Constant as C

-- | Set of @Html5@ elements that do not allow content.
html5_empty_elem :: [String]
html5_empty_elem =
  [ "area"
  , "base"
  , "br"
  , "col"
  , "command"
  , "embed"
  , "hr"
  , "img"
  , "input"
  , "keygen"
  , "link"
  , "meta"
  , "param"
  , "source"
  , "track"
  , "wbr"
  ]

pp_reconf :: X.ConfigPP -> X.ConfigPP
pp_reconf =
  let f nm = X.qName nm `elem` html5_empty_elem
  in X.useShortEmptyTags f

pp_element :: X.ConfigPP -> X.Element -> String
pp_element c = X.ppcElement (pp_reconf c)

pp_element_def :: X.Element -> String
pp_element_def = pp_element X.defaultConfigPP

-- | Render an XHtml element with the given document type.
renderXHtml :: C.DocType -> X.Element -> String
renderXHtml t e = concat [C.xml_1_0, t, pp_element_def e]

-- | Render an Html5 element.
renderHtml5_def :: X.Element -> String
renderHtml5_def e = concat [C.html5_dt, pp_element_def e]

-- | Pretty-printing variant (inserts whitespace).
renderHtml5_pp :: X.Element -> String
renderHtml5_pp e = unlines [C.html5_dt, pp_element X.prettyConfigPP e]

-- | Show Html content (importantly an 'iframe' element must not be abbreviated).
showHtml5 :: X.Content -> String
showHtml5 = X.ppcContent (pp_reconf X.defaultConfigPP)
