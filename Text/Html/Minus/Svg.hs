-- | Svg.
module Text.Html.Minus.Svg where

import Text.Printf {- base -}

import Text.Html.Minus.Attribute (Attr, mk_attr {- html-minus -})
import Text.Html.Minus.Element (Content, cdata_raw, mk_element, mk_empty_element {- html-minus -})

-- * Type

type R = Double

-- * Constants

xmlns_2000_svg :: String
xmlns_2000_svg = "http://www.w3.org/2000/svg"

-- * Attributes

font_family :: String -> Attr
font_family = mk_attr "font-family"

font_size :: R -> Attr
font_size = mk_attr "font-size" . show

height :: R -> Attr
height = mk_attr "height" . show

href :: String -> Attr
href = mk_attr "href"

stroke_rgb :: (R, R, R) -> Attr
stroke_rgb (r, g, b) =
  let f :: R -> Int
      f n = floor (n * 255)
  in mk_attr "stroke" (printf "rgb(%d,%d,%d)" (f r) (f g) (f b))

stroke_width :: R -> Attr
stroke_width = mk_attr "stroke-width" . show

target :: String -> Attr
target = mk_attr "target"

viewBox :: (R, R, R, R) -> Attr
viewBox (min_x, min_y, w, h) = mk_attr "viewBox" (printf "%f %f %f %f" min_x min_y w h)

width :: R -> Attr
width = mk_attr "width" . show

x :: R -> Attr
x = mk_attr "x" . show

x1 :: R -> Attr
x1 = mk_attr "x1" . show

x2 :: R -> Attr
x2 = mk_attr "x2" . show

xmlns :: String -> Attr
xmlns = mk_attr "xmlns"

y :: R -> Attr
y = mk_attr "y" . show

y1 :: R -> Attr
y1 = mk_attr "y1" . show

y2 :: R -> Attr
y2 = mk_attr "y2" . show

-- * Elements

-- | anchor element
a :: [Attr] -> [Content] -> Content
a = mk_element "a"

{- | line element

> line [x1 0,y1 0,x2 100,y2 100,stroke_width 0.1,stroke_rgb (0.5,0.5,0.5)]
-}
line :: [Attr] -> Content
line = mk_empty_element "line"

{- | svg element

> svg [width 120,height 120,viewBox (0,0,120,120),xmlns xmlns_2000_svg] []
-}
svg :: [Attr] -> [Content] -> Content
svg = mk_element "svg"

-- | text element
text :: [Attr] -> [Content] -> Content
text = mk_element "text"

{- | text element, content from string, wrapped in 'cdata_raw'.

> a [href "x"] [text_str [x 0, y 35, font_family "Verdana",font_size 35] "text"]
-}
text_str :: [Attr] -> String -> Content
text_str attr str = text attr [cdata_raw str]
