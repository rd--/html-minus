-- | Composite constructors and constants.
module Text.Html.Minus.Composite where

import Data.Char {- base -}
import Data.List as L {- base -}
import Data.Maybe {- base -}

import qualified Text.XML.Light as X {- xml -}

import qualified Text.Html.Minus.Attribute as H
import qualified Text.Html.Minus.Element as H

-- * Attributes

-- | Variant on 'X.add_attrs' lifted to 'X.Content'.
x_add_attrs_content :: [H.Attr] -> H.Content -> H.Content
x_add_attrs_content z c =
  case c of
    X.Elem e -> X.Elem (X.add_attrs z e)
    _ -> error "x_add_attrs_content: not Elem"

-- * Html

html_en :: [H.Content] -> H.Element
html_en = H.html [H.lang "en"]

-- * Class/Id

-- | Element with @class@ attribute.
elem_c :: ([H.Attr] -> t) -> String -> t
elem_c f c = f [H.class_attr c]

body_c :: String -> [H.Content] -> H.Content
body_c = elem_c H.body

div_c :: String -> [H.Content] -> H.Content
div_c = elem_c H.div

span_c :: String -> [H.Content] -> H.Content
span_c = elem_c H.span

ul_c :: String -> [H.Content] -> H.Content
ul_c = elem_c H.ul

li_c :: String -> [H.Content] -> H.Content
li_c = elem_c H.li

p_c :: String -> [H.Content] -> H.Content
p_c = elem_c H.p

attr_cid :: (String, String) -> [H.Attr]
attr_cid (c, k) = [H.class_attr c, H.id k]

-- | Element with @class@ and @id@ attributes.
elem_cid :: ([H.Attr] -> t) -> (String, String) -> t
elem_cid f cid = f (attr_cid cid)

div_cid :: (String, String) -> [H.Content] -> H.Content
div_cid = elem_cid H.div

span_cid :: (String, String) -> [H.Content] -> H.Content
span_cid = elem_cid H.span

ol_cid :: (String, String) -> [H.Content] -> H.Content
ol_cid = elem_cid H.ol

ul_cid :: (String, String) -> [H.Content] -> H.Content
ul_cid = elem_cid H.ul

li_cid :: (String, String) -> [H.Content] -> H.Content
li_cid = elem_cid H.li

p_cid :: (String, String) -> [H.Content] -> H.Content
p_cid = elem_cid H.p

-- * Meta elements

-- | Set @content-type@.
meta_content_type :: String -> H.Content
meta_content_type t = H.meta [H.http_equiv "content-type", H.content t]

-- | Set @author@.
meta_author :: String -> H.Content
meta_author au = H.meta [H.name "author", H.content au]

-- | Set @charset@.
meta_charset :: String -> H.Content
meta_charset ch = H.meta [H.charset ch]

-- | Set @description@.
meta_description :: String -> H.Content
meta_description dsc = H.meta [H.name "description", H.content dsc]

-- | Set @meta_viewport@.
meta_viewport :: String -> H.Content
meta_viewport v = H.meta [H.name "viewport", H.content v]

-- * Link elements

-- | Enumeration of Html5 link types.
data Link_Type
  = Link_Alternate
  | Link_Author
  | Link_Bookmark
  | Link_Help
  | Link_Icon
  | Link_License
  | Link_Next
  | Link_NoFollow
  | Link_NoReferrer
  | Link_Prefetch
  | Link_Prev
  | Link_Search
  | Link_Stylesheet
  deriving (Eq, Show)

{- | Html name for 'Link_Type'.

>>> link_type_str Link_Stylesheet
"stylesheet"
-}
link_type_str :: Link_Type -> String
link_type_str = L.map toLower . fromJust . stripPrefix "Link_" . show

-- | Variant on 'link' with enumerated /type/ value.
link_ty :: Link_Type -> [H.Attr] -> H.Content
link_ty ty = H.link . (:) (H.rel (link_type_str ty))

-- | Set CSS @stylesheet@ for given @media@.
link_css :: String -> String -> H.Content
link_css m c =
  link_ty
    Link_Stylesheet
    [ H.type_attr "text/css"
    , H.media m
    , H.href c
    ]

-- | Set RSS @alternate@ with given @title@.
link_rss :: String -> String -> H.Content
link_rss tt ln =
  link_ty
    Link_Alternate
    [ H.type_attr "application/rss+xml"
    , H.title_attr tt
    , H.href ln
    ]

-- * Script elements

-- | Embed javascript.
script_js :: String -> H.Content
script_js s = H.script [H.type_attr "text/javascript"] [H.cdata_raw s]

-- | Source javascript (the type attribute is unnecessary for JavaScript resources).
script_js_src :: FilePath -> H.Content
script_js_src fn = H.script [H.src fn] []

-- * Validators

-- | @W3.org@ Html validator.
w3_html_validator :: String
w3_html_validator = "http://validator.w3.org/check/referer"

-- | @W3.org@ CSS validator.
w3_css_validator :: String
w3_css_validator = "http://jigsaw.w3.org/css-validator/check/referer"

{- | @W3.org@ CSS validator.

>>> w3_rss_validator "http://haskell.org"
"http://validator.w3.org/feed/check.cgi?url=http://haskell.org"
-}
w3_rss_validator :: String -> String
w3_rss_validator = (++) "http://validator.w3.org/feed/check.cgi?url="

-- * Input elements

-- | Enumeration of Html5 input types.
data Input_Type
  = Hidden
  | Text
  | Search
  | Tel
  | Url
  | Email
  | Password
  | DateTime
  | Date
  | Month
  | Week
  | Time
  | DateTime_Local
  | Number
  | Range
  | Color
  | Checkbox
  | Radio
  | File
  | Submit
  | Image
  | Reset
  | Button
  deriving (Eq, Show)

{- | Html name for 'Input_Type'.

>>> input_type_str Hidden
"hidden"
-}
input_type_str :: Input_Type -> String
input_type_str =
  let f c = if c == '_' then '-' else c
  in L.map (f . toLower) . show

{- | Variant on 'input' with enumerated /type/ value.

>>> import Text.Html.Minus.Render
>>> putStr $ showHtml5 $ input_ty Button []
<input type="button" />
-}
input_ty :: Input_Type -> [H.Attr] -> H.Content
input_ty ty = H.input . (:) (H.type_attr (input_type_str ty))

-- | 'Hidden' input with /name/ and /value/.
input_hidden :: String -> String -> H.Content
input_hidden k v = input_ty Hidden [H.name k, H.value v]

-- | 'Submit' input with /name/ and /value/.
input_submit :: String -> String -> H.Content
input_submit k v = input_ty Submit [H.name k, H.value v]
