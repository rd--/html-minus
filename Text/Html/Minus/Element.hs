{- | Element constructors.  The resulting elements are lifted to the
  Content data type, with the exception of the 'html' element.
-}
module Text.Html.Minus.Element where

import qualified Text.XML.Light as X {- xml -}

{-
import Data.String {- base -}

instance IsString X.Content where
    fromString = cdata
-}

-- * Content constructors

type Content = X.Content

-- | Ordinary character data, subject to escaping.
cdata :: String -> Content
cdata s = X.Text (X.CData X.CDataText s Nothing)

-- | Raw character data, not subject to escaping.
cdata_raw :: String -> Content
cdata_raw s = X.Text (X.CData X.CDataRaw s Nothing)

-- * Unlifted element constructors

type Element = X.Element

mk_element_e :: String -> [X.Attr] -> [Content] -> Element
mk_element_e nm z e = X.Element (X.unqual nm) z e Nothing

html :: [X.Attr] -> [Content] -> Element
html = mk_element_e "html"

-- * Lifted element constructors

-- | Element contructor.
type Element_C = [X.Attr] -> [Content] -> Content

-- | Empty element contructor.
type Empty_Element_C = [X.Attr] -> Content

mk_element :: String -> Element_C
mk_element nm z = X.Elem . mk_element_e nm z

mk_empty_element :: String -> Empty_Element_C
mk_empty_element nm z = X.Elem (X.Element (X.unqual nm) z [] Nothing)

-- * Elements

a :: Element_C
a = mk_element "a"

address :: Element_C
address = mk_element "address"

area :: Empty_Element_C
area = mk_empty_element "area"

article :: Element_C
article = mk_element "article"

aside :: Element_C
aside = mk_element "aside"

audio :: Element_C
audio = mk_element "audio"

base :: Empty_Element_C
base = mk_empty_element "base"

body :: Element_C
body = mk_element "body"

br :: Empty_Element_C
br = mk_empty_element "br"

button :: Element_C
button = mk_element "button"

caption :: Element_C
caption = mk_element "caption"

cite :: Element_C
cite = mk_element "cite"

colgroup :: Element_C
colgroup = mk_element "colgroup"

col :: Empty_Element_C
col = mk_empty_element "col"

datalist :: Element_C
datalist = mk_element "datalist"

dd :: Element_C
dd = mk_element "dd"

details :: Element_C
details = mk_element "details"

dialog :: Element_C
dialog = mk_element "dialog"

div :: Element_C
div = mk_element "div"

dl :: Element_C
dl = mk_element "dl"

dt :: Element_C
dt = mk_element "dt"

em :: Element_C
em = mk_element "em"

embed :: Empty_Element_C
embed = mk_empty_element "embed"

fieldset :: Element_C
fieldset = mk_element "fieldset"

figcaption :: Element_C
figcaption = mk_element "figcaption"

figure :: Element_C
figure = mk_element "figure"

footer :: Element_C
footer = mk_element "footer"

form :: Element_C
form = mk_element "form"

frame :: Empty_Element_C
frame = mk_empty_element "frame"

frameset :: Empty_Element_C
frameset = mk_empty_element "frameset"

h1 :: Element_C
h1 = mk_element "h1"

h2 :: Element_C
h2 = mk_element "h2"

h3 :: Element_C
h3 = mk_element "h3"

h4 :: Element_C
h4 = mk_element "h4"

head :: Element_C
head = mk_element "head"

header :: Element_C
header = mk_element "header"

hgroup :: Element_C
hgroup = mk_element "hgroup"

hr :: Empty_Element_C
hr = mk_empty_element "hr"

iframe :: Element_C
iframe = mk_element "iframe"

img :: Empty_Element_C
img = mk_empty_element "img"

input :: Empty_Element_C
input = mk_empty_element "input"

label :: Element_C
label = mk_element "label"

legend :: Element_C
legend = mk_element "legend"

li :: Element_C
li = mk_element "li"

link :: Empty_Element_C
link = mk_empty_element "link"

map :: Element_C
map = mk_element "map"

meta :: Empty_Element_C
meta = mk_empty_element "meta"

meter :: Empty_Element_C
meter = mk_empty_element "meter"

nav :: Element_C
nav = mk_element "nav"

object :: Element_C
object = mk_element "object"

optgroup :: Element_C
optgroup = mk_element "optgroup"

option :: Element_C
option = mk_element "option"

ol :: Element_C
ol = mk_element "ol"

p :: Element_C
p = mk_element "p"

param :: Empty_Element_C
param = mk_empty_element "param"

pre :: Element_C
pre = mk_element "pre"

progress :: Element_C
progress = mk_element "progress"

q :: Element_C
q = mk_element "q"

script :: Element_C
script = mk_element "script"

section :: Element_C
section = mk_element "section"

select :: Element_C
select = mk_element "select"

source :: Empty_Element_C
source = mk_empty_element "source"

span :: Element_C
span = mk_element "span"

strong :: Element_C
strong = mk_element "strong"

style :: Element_C
style = mk_element "style"

sub :: Element_C
sub = mk_element "sub"

summary :: Element_C
summary = mk_element "summary"

sup :: Element_C
sup = mk_element "sup"

table :: Element_C
table = mk_element "table"

tbody :: Element_C
tbody = mk_element "tbody"

td :: Element_C
td = mk_element "td"

textarea :: Element_C
textarea = mk_element "textarea"

tfoot :: Element_C
tfoot = mk_element "tfoot"

th :: Element_C
th = mk_element "th"

thead :: Element_C
thead = mk_element "thead"

time :: Element_C
time = mk_element "time"

title :: Element_C
title = mk_element "title"

tr :: Element_C
tr = mk_element "tr"

ul :: Element_C
ul = mk_element "ul"

video :: Element_C
video = mk_element "video"

wbr :: Empty_Element_C
wbr = mk_empty_element "wbr"
