all:
	ghc -O --make Text/Html/Minus.hs

mk-cmd:
	echo "html-minus - nil"

ln-local:
	rm -f Text/XML
	ln -s $(HOME)/opt/src/xml-1.3.9/Text/XML Text/XML

clean:
	rm -Rf dist dist-newstyle *~
	find . -name '*.hi' -o -name '*.o' | xargs rm -f

remote-update:
	ssh rd@rohandrape.net "(cd ~/sw/html-minus ; make clean all ; cd ~/sw/www-minus ; make clean ln-local all)"

push-all:
	r.gitlab-push.sh html-minus

indent:
	fourmolu -i Text

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Text
